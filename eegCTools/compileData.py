#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
compileData
Compile simulation data.
"""

import os

import numpy as np
import scipy.io
import scipy.signal
from matplotlib import pyplot as plt
from scipy.io.matlab import savemat


def compileData(path, simID, subjects, nsur):
    # init compilation
    file = path + simID + "_C01_SJ01.mat"
    Data = scipy.io.loadmat(file, simplify_cells=True, squeeze_me=True)

    # parse info
    opt = Data["opt"]
    nrea = opt["nrea"]
    nsur = opt["nsur"]

    # OC condition
    OC = {}
    txy, tyx, txys, tyxs, seeds, fnames = getSimData(path, simID, "OC",
                                                     subjects, nrea, nsur)
    OC["txy"] = txy
    OC["tyx"] = tyx
    OC["txys"] = txys
    OC["tyxs"] = tyxs
    opt["seedsOC"] = seeds
    opt["fnamesOC"] = fnames

    # # OA condition
    OA = {}
    txy, tyx, txys, tyxs, seeds, fnames = getSimData(path, simID, "OA",
                                                     subjects, nrea, nsur)
    OA["txy"] = txy
    OA["tyx"] = tyx
    OA["txys"] = txys
    OA["tyxs"] = tyxs
    opt["seedsOA"] = seeds
    opt["fnamesOA"] = fnames

    # save
    data = {}
    data["OC"] = OC
    data["OA"] = OA
    data["opt"] = opt
    data["matDims"] = "nsubj x nwindows x nchanPairs x nsurrogates"

    OUTDIR = path + "CompiledData/"
    if not os.path.isdir(OUTDIR):
        os.makedirs(OUTDIR)
        print("created folder : ", OUTDIR)

    oname = "{0}_CompiledData.mat".format(simID)
    savemat(OUTDIR + oname, data)

    return 0


def getSimData(path, simID, condition, subjects, nrea, nsur):

    if condition == "OC":
        cv = 1
    else:
        cv = 2
    nsubj = len(list(subjects))

    txy = np.zeros((nsubj, nrea, nsur))
    tyx = np.zeros(txy.shape)
    txys = np.zeros(txy.shape)
    tyxs = np.zeros(txy.shape)

    seed = []
    fname = []
    for i in subjects:
        if i < 10:
            file = "{0}{1}_C0{2}_SJ0{3}.mat".format(path, simID, cv, i)
            fileSur = "{0}surrogates/{1}s_C0{2}_SJ0{3}.mat".format(
                path, simID, cv, i)
        else:
            file = "{0}{1}_C0{2}_SJ{3}.mat".format(path, simID, cv, i)

        Data = scipy.io.loadmat(file, simplify_cells=True, squeeze_me=True)
        seed.append(Data["opt"]["seed"])
        # fname.append(Data["opt"]["fname"])
        txy[i - 1, :, :] = Data["data"]["txy"]
        tyx[i - 1, :, :] = Data["data"]["tyx"]

        # read surrogates
        Data = scipy.io.loadmat(fileSur, simplify_cells=True, squeeze_me=True)
        txys[i - 1, :, :] = Data["data"]["txys"]
        tyxs[i - 1, :, :] = Data["data"]["tyxs"]

    return txy, tyx, txys, tyxs, seed, fname


if __name__ == "__main__":

    for i in [10, 11, 12, 13, 14]:
        simConf = {
            "path": f"/media/jrestrepo/Datos/eegControl/sims/sim{i}/",
            "simID": f"PP{i}",
            "subjects": range(1, 11),
            "nsur": 512,
        }
        A = compileData(**simConf)
