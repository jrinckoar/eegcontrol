function M = vecToMat09(V, chInds)

nch = 20;
[nsubj, nrea] = size(V);

M = zeros(nsubj, nch, nch);

for s=1:nsubj
    for r =1:nrea
        M(s, chInds(1,r), chInds(2,r)) = V(s,r);
    end
end
