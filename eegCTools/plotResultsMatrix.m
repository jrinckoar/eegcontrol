function plotResultsMatrix(M, blimit, tlimit, chLabels)
nch = 20;
ticks = 1:nch;
imagesc(ticks, ticks, M);
hold on;
plot([8.5 8.5],[0 21], 'k','LineWidth', 2)
plot([12.5 12.5],[0 21], 'k','LineWidth', 2)
plot([0, 21], [8.5 8.5], 'k','LineWidth', 2)
plot([0 21], [12.5 12.5], 'k','LineWidth', 2)
hold off
set(gca, 'YTick', ticks, 'YTickLabel', chLabels)
set(gca, 'XTick', ticks, 'XTickLabel', chLabels)
xlabel('Y')
ylabel('X')
caxis([blimit tlimit]);
colorbar;
