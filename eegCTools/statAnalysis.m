function [Tent, Flow, Coupling]=statAnalysis10(Tx, Ty, Txs, Tys, alpha)

[nsubj, nrea, nsur] = size(Txs);

Flow = zeros(nsubj, nrea);
Tent = zeros(nsubj, nrea);
Coupling = zeros(nsubj, nrea);

tolc = nsur - floor(alpha * nsur);
tol1 = floor(0.5*alpha * nsur);
tol2 = nsur - tol1;


for s=1:nsubj
    for r =1:nrea
        tx = squeeze(Tx(s,r,:));
        txs = squeeze(Txs(s,r,:));
        ty = squeeze(Ty(s,r,:));
        tys = squeeze(Tys(s,r,:));

        d = abs(tx) - abs(ty);
        ds = abs(txs) - abs(tys);
        %d= tx - ty;
        %ds = txs - tys;
        dsm = median(ds);
%         d = d - dsm;
%         ds = ds - dsm;
        dm = median(d);

%         figure(1)
%         histogram(ds, 32, 'FaceColor','b');
%         hold on;
%         histogram(d, 32, 'FaceColor','r');
%         hold off;
%         title('txy - tyx')

        
        %test de doble cola
        dsort = sort([ds; dm], 'ascend');
        ind = find(dsort == dm);
        if ind < tol1  % tyx > txy
            c = coupling(tx,ty,txs,tys,tolc);
            Coupling(s,r) = c;
            if c ~= 0
                Flow(s,r) = -1;
                Tent(s,r) = dm - dsm; 
            end
        end
        if ind > tol2   % txy > tyx
            c = coupling(tx,ty,txs,tys,tolc);
            Coupling(s,r) = c;
            if c ~= 0
                Flow(s,r) = 1;
                Tent(s,r) = dm - dsm; 
            end
        end
    end
end
end


function C = coupling(tx, ty, txs, tys, tol)

    %mxs = median(txs);
    %mys = median(tys);
    %tx = tx - mxs;
    %ty = ty - mys;
    %txs = txs - mxs;
    %tys = tys - mys;

    mx = median(tx);
    my = median(ty);

%     figure(2)
%     histogram(txs, 32, 'FaceColor','b');
%     hold on;
%     histogram(tx, 32, 'FaceColor','r');
%     hold off;
%     title('txy')
%     figure(3)
%     histogram(tys, 32, 'FaceColor','b');
%     hold on;
%     histogram(ty, 32, 'FaceColor','r');
%     hold off;
%     title('tyx')

    %test de simple cola txy
    dsortx = sort([mx; txs], 'ascend');
    indx = find(dsortx == mx,1,'first');
    C = 0;
    if indx > tol   % txy > txys
        C = C + 1;
    end
    %test de simple cola txy
    dsorty = sort([my; tys], 'ascend');
    indy = find(dsorty == my,1, 'first');
    if indy > tol   % tyx > tyxs
        C = C + 1;
    end
    


end




