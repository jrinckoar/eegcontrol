#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
eegSurrogates
Surrogate data generators
"""

import numpy as np
import ray
from knnIMpy.knnTent import knnTent01 as tf
from matplotlib import pyplot as plt
from scipy.fftpack import fft, ifft

# from scipy.signal import hilbert


def cropWindows(x, wlength, suj, cond):
    IND = {
        'OC': [0, 6, 6.4, 6, 0, 3, 4, 6, 6, 3.5, 5, 3, 4, 6, 6],
        'OA': [5, 0, 0, 0, 3.8, 0, 5.5, 0, 1, 2.3, 3, 1, 1.4, 6, 6]
    }
    ind = int(IND[cond][suj]) * 1000
    return x[:, ind:ind + wlength]


def fourierSurrogates(x, ax):
    """
     Generate random phase fourier surrogate data
     x: data
     ax: axis of data
     return: random phase fourier surrogate data
    """
    X = np.abs(fft(x, axis=ax))
    randPhase = np.angle(fft(np.random.randn(*x.shape), axis=ax))
    xs = np.real(ifft(X * np.exp(1j * randPhase), axis=ax))
    return xs
    # np.angle(hilbert(xs, axis=ax))


def circularShiftSurrogate(x, ax):
    """
     Generate random circular shift surrogate data
     x: data
     ax: axis of data
     return: circular shift surrogate
    """
    n = np.max(x.shape)
    ind = np.random.randint(0, n, 1)
    xs = np.roll(x, ind, axis=ax)
    return xs


def shiftSurrogate(x, ax):
    """
     Generate random circular shift surrogate data
     x: data
     ax: axis of data
     return: circular shift surrogate
    """
    n = np.max(x.shape)
    xs = x.copy()
    for row, col in enumerate(x):
        xs[row, :] = np.random.permutation(col)
    return xs


@ray.remote
def calc_tent(tf_opt, indSorce, indTarget, x, xs=None):
    """
        Calculate transfer
    """
    if xs is None:
        return tf(x[indSorce, :], x[indTarget, :], **tf_opt)
    else:
        return tf(xs[indSorce, :], x[indTarget, :], **tf_opt)  # surrogates


def embedding(x, m, tau):
    """
    Time series embedding
    """
    x = x.reshape(x.shape[0], -1)
    if x.shape[0] < x.shape[1]:
        x = x.T
    n, k = x.shape
    l_ = n - (m - 1) * tau
    V = np.zeros((l_, m * k))

    for j in range(k):
        for i in range(m):
            V[:, i + j * m] = x[i * tau:i * tau + l_, j]
    return V


def plotEmebedding(source, target, m, tau, u):
    sv = embedding(source, m, tau)
    tv = embedding(target, m, tau)
    N = min(sv.shape[0], tv.shape[0])
    sv = sv[:N, :]
    tv = tv[:N, :]
    fig = plt.figure()
    # fig2 = plt.figure()
    fig3 = plt.figure()
    ax = fig.gca(projection='3d')
    ax.scatter(sv[:, 0], sv[:, 1], sv[:, 2])
    # ax.scatter(sv[:,0], sv[:,1])
    # ax2 = fig2.gca(projection='3d')
    # ax2.scatter(tv[:,0], tv[:,1])
    # ax2.scatter(tv[:, 0], tv[:, 1], tv[:, 2])
    ax3 = fig3.gca(projection='3d')
    ax3.scatter(tv[u:, 0], tv[:-u, 0], sv[:-u, 0])
    plt.show()
