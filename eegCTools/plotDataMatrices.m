%{
Flujo:  1   Flujo de X -> Y
        -1  Flujo de Y -> X
Acople: 1 unidireccional.
        2 bidireccional. 
%}

load('Sim12_alphaBand.mat')
% Graficar por sujeto
for i =1:10
    SUJETO=i;
    OCd = squeeze(OC.flow(SUJETO,:,:));
    OAd = squeeze(OA.flow(SUJETO,:,:));
    OCc = squeeze(OC.coupling(SUJETO,:,:));
    OAc = squeeze(OA.coupling(SUJETO,:,:));

    figure(10);
    subplot(121)
    plotResultsMatrix(OCd, -1, 1, chnames)
    title('flujo')
    subplot(122)
    plotResultsMatrix(OCc, 0, 2, chnames)
    sgtitle(['Sim12 alpha - Txy - Tyx - Ojos Cerrados ', ' - Sujeto ' num2str(SUJETO)])
    title('acople')
    figure(11);
    subplot(121)
    plotResultsMatrix(OAd, -1, 1, chnames)
    title('flujo')
    subplot(122)
    plotResultsMatrix(OAc, 0, 2, chnames)
    title('acople')
    sgtitle(['Sim12 alpha - Txy - Tyx Ojos Abiertos ', ' - Sujeto ' num2str(SUJETO)])
    pause % presionar enter para ver los otros sujetos
end
