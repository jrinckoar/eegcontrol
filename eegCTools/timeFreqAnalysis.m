%{
----------------------------------------------------------------------------
    Time frequency Analysis
----------------------------------------------------------------------------

Data

EEG records 19 channels.

Control 230979 data points (15.4 minutes, Fs=250 Hz).
Ezq 210436 data points (14.03 minutes, Fs=250 Hz)


'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-10-15
%}


%% Load Data
clear;
clc;
% Raw data
dataPathOA = '/home/jrestrepo/Datos/eegEqz/GrupoControl/mat/VigiliaOjosAbiertos/';
dataPathOC = '/home/jrestrepo/Datos/eegEqz/GrupoControl/mat/VigiliaOjosCerrados/';


data = load([dataPathOA, 'ID_01_TF_OA.mat']);
chNames = data.chanlocs;
nch = data.nch;
Fs = double(data.fs);
clear data;
 
% Load time ind
timeIndexFileOA = './GrupoControlVentanas_30s_OjosAbiertos.csv';
T = readtable(timeIndexFileOA,'NumHeaderLines',1);
tOA = T{:,2:end};

timeIndexFileOC = './GrupoControlVentanas_30s_OjosCerrados.csv';
T = readtable(timeIndexFileOC,'NumHeaderLines',1);
tOC = T{:,2:end};


%% eeg FFT 
for i=1:10
    i
    if i < 10
        str = '_0';
    else
        str = '_';
    end
    data = load([dataPathOA, 'ID', str, num2str(i) , '_TF_OA.mat']);
    dataOA = data.eeg;
    data = load([dataPathOC, 'ID', str, num2str(i) , '_TF_OC.mat']);
    dataOC = data.eeg;

    % Normalizar los datos (media cero y varianza unitaria)
    dataOA = (dataOA - mean(dataOA, 1)) ./ std(dataOA, [], 1);
    dataOC = (dataOC - mean(dataOC, 1)) ./ std(dataOC, [], 1);


    % Graficar espectros
    figure(1);
    fOA = linspace(-0.5, 0.5, length(dataOA)) * Fs; % Vector de frecuencias.
    fOC = linspace(-0.5, 0.5, length(dataOC)) * Fs;
    subplot(211)
    AmpOA = abs(fftshift(fft(dataOA, [], 1))); % espectros de amplitud
    AmpOC = abs(fftshift(fft(dataOC, [], 1)));
    plot(fOA, AmpOA ./ length(dataOA));
    title(['Amplitud Ojos Abiertos ', num2str(i)]);
    xlabel('f Hz');
    subplot(212)
    plot(fOC, AmpOC ./length(dataOC));
    title(['Amplitud Ojos Cerrados ', num2str(i)]);
    xlabel('f');

    % Graficar series temporales
    figure(2)
    subplot(211)
    plot(dataOA);
    hold on;
    a = max(dataOA(:));
    plot([tOA(i,1) tOA(i,2)], [a a],'r', 'LineWidth', 4);
    plot([tOA(i,3) tOA(i,4)], [a a],'r', 'LineWidth', 4);
    hold off
    title(['Ojos Abiertos ', num2str(i)]);
    xlabel('n');
    subplot(212)
    plot(dataOC);
    hold on;
    a = max(dataOC(:));
    plot([tOC(i,1) tOC(i,2)], [a a],'r', 'LineWidth', 4);
    plot([tOC(i,3) tOC(i,4)], [a a],'r', 'LineWidth', 4);
    hold off
    title(['Ojos cerrados ', num2str(i)]);
    xlabel('n');
    pause
end
%% Windows eeg
for i=1:10
    i
    if i < 10
        str = '_0';
    else
        str = '_';
    end
    data = load([dataPathOA, 'ID', str, num2str(i) , '_TF_OA.mat']);
    dataOA = data.eeg;
    data = load([dataPathOC, 'ID', str, num2str(i) , '_TF_OC.mat']);
    dataOC = data.eeg;
    
    dataOAw1 = dataOA(tOA(i,1):tOA(i,2),:);
    dataOAw2 = dataOA(tOA(i,3):tOA(i,4),:);

    dataOCw1 = dataOC(tOC(i,1):tOC(i,2),:);
    dataOCw2 = dataOC(tOC(i,3):tOC(i,4),:);
    
    dataOAw1 = (dataOAw1 - mean(dataOAw1, 1)) ./ std(dataOAw1, [], 1);
    dataOAw2 = (dataOAw2 - mean(dataOAw2, 1)) ./ std(dataOAw2, [], 1);

    dataOCw1 = (dataOCw1 - mean(dataOCw1, 1)) ./ std(dataOCw1, [], 1);
    dataOCw2 = (dataOCw2 - mean(dataOCw2, 1)) ./ std(dataOCw2, [], 1);

    figure(1);
    fOA = linspace(-0.5, 0.5, length(dataOAw1)) * Fs;
    subplot(211)
    AmpOAw1 = abs(fftshift(fft(dataOAw1, [], 1)));
    AmpOAw2 = abs(fftshift(fft(dataOAw2, [], 1)));
    plot(fOA, AmpOAw1 ./ length(dataOAw1));
    title(['Amplitud Ojos Abiertos W1 ', num2str(i)]);
    xlabel('f Hz');
    subplot(212)
    plot(fOA, AmpOAw2 ./length(dataOAw2));
    title(['Amplitud Ojos Abiertos W2 ', num2str(i)]);
    xlabel('f');

    figure(2);
    fOC = linspace(-0.5, 0.5, length(dataOCw1)) * Fs;
    subplot(211)
    AmpOCw1 = abs(fftshift(fft(dataOCw1, [], 1)));
    AmpOCw2 = abs(fftshift(fft(dataOCw2, [], 1)));
    plot(fOC, AmpOCw1 ./ length(dataOCw1));
    title(['Amplitud Ojos Cerrados W1 ', num2str(i)]);
    xlabel('f Hz');
    subplot(212)
    plot(fOC, AmpOCw2 ./length(dataOCw2));
    title(['Amplitud Ojos Cerrados W2 ', num2str(i)]);
    xlabel('f');


    figure(3)
    subplot(211)
    plot(dataOAw1);
    title(['Ojos Abiertos ', num2str(i), 'w1']);
    xlabel('n');
    subplot(212)
    plot(dataOAw2);
    title(['Ojos Abiertos ', num2str(i), 'w2']);
    xlabel('n');

    
    figure(4)
    subplot(211)
    plot(dataOCw1);
    title(['Ojos Cerrados ', num2str(i), 'w1']);
    xlabel('n');
    subplot(212)
    plot(dataOCw2);
    title(['Ojos Cerrados ', num2str(i), 'w2']);
    xlabel('n');
    pause
end




%% Time-Frequency filterbank
% paramters
t = 0:n - 1;
Wavelet = 'amor';
FrequencyLimits = [0.1, Fs/2]; % 6 hours to 3 months.
VoicesPerOctave = 10;
fb = cwtfilterbank('SignalLength', n, 'Wavelet', Wavelet, ...
    'VoicesPerOctave', VoicesPerOctave, 'SamplingFrequency', Fs, ...
    'FrequencyLimits', FrequencyLimits);

for i=1: nch
    figure(3)
    cwt(dataControl(:,i), 'FilterBank', fb);
    title(['CONTROL - cwt - morlet ch ', chNames{i}])
    figure(4)
    cwt(dataEqz(:,i), 'FilterBank', fb);
    title(['EZQ - cwt - morlet ch ', chNames{i}])
    i
    pause
end

