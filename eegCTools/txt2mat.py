#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
txt2mat: Take eeg signals in txt and make the matlab data array.
"""

import os
from datetime import datetime

import numpy as np
import pandas as pd
from scipy.io.matlab.mio import savemat

ROOT_DIR = '/media/jrestrepo/Datos/eegControl/GrupoControl/'
TXT_DATA_DIR = f"{ROOT_DIR}txt/"
MAT_DATA_DIR = f"{ROOT_DIR}mat/"
OA_DIR = 'VigiliaOjosAbiertos/'
OC_DIR = 'VigiliaOjosCerrados/'
SAMPLING_FRECUENCY = 65
NUMBER_OF_CHANELS = 20
NSUBJECTS = 15
EEG_START_IND = 6
CONDITION = {'OA': 'Ojos Abiertos', 'OC': 'Ojos Cerrados'}
CONDITION_DIR = {'OA': OA_DIR, 'OC': OC_DIR}


def save2mat(dataDict, condition, subj, fname):
    now = datetime.now()
    current_time = now.strftime("%d/%m/%Y %H:%M:%S")
    infoText = [
        'Equipo marca Neutronic', 'La referencia es Bimastoidea,',
        'los canales F1-F3-C3-P3-O1-F7-T3-T5-Fz'
        'y Oz referencian a A1 (Referencia izquierda)', 'y el resto a A2.',
        f'Frecuencia de muestreo: {SAMPLING_FRECUENCY} Hz.',
        f'Número de canales: {NUMBER_OF_CHANELS}.',
        f'Sujeto {subj} de {NSUBJECTS}.',
        f'Condición: {CONDITION[condition]}.',
        f'Datos compilados en .mat {current_time}.'
    ]
    dataDict["info"] = infoText
    dataDict["fs"] = SAMPLING_FRECUENCY
    dataDict["nch"] = NUMBER_OF_CHANELS
    dataDict['condition'] = condition
    dataDict['subj'] = subj
    dataDict["chanlocs"] = np.array([
        "F1", "F3", "C3", "P3", "O1", "F7", "T3", "T5", "Fz", "Cz", "Pz", "Oz",
        "T6", "T4", "F8", "O2", "P4", "C4", "F4", "F2"
    ],
                                    dtype=object)
    opath = f'{MAT_DATA_DIR}{CONDITION_DIR[condition]}'
    if not os.path.isdir(opath):
        os.makedirs(opath)
        print(f"Folder Creado: {opath}")
    ofname = f'{opath}{fname}.mat'
    savemat(ofname, dataDict)


def txt2mat():
    nch = NUMBER_OF_CHANELS
    eegStartInd = EEG_START_IND
    for condition in CONDITION.keys():
        dataDict = {}
        for subj in range(1, NSUBJECTS + 1):
            if subj < 10:
                subject = f'0{subj}'
            else:
                subject = f'{subj}'
            fname = f'ID_{subject}_TF_{condition}'
            inpath = f'{TXT_DATA_DIR}{CONDITION_DIR[condition]}'
            df = pd.read_csv(inpath + fname + '.txt', sep=" ", index_col=False)
            n, _ = df.shape
            eeg = np.zeros((n - 1, nch))
            for i in range(nch):
                eeg[:, i] = df.iloc[1:, eegStartInd + i]

            # remover Nan
            eeg_trimmed = eeg[~np.isnan(eeg)].reshape(-1, nch)

            dataDict["eeg"] = eeg_trimmed
            save2mat(dataDict, condition, subj, fname)


if __name__ == "__main__":
    txt2mat()
