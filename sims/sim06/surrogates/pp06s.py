#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
EEG Control - transfer entropy

sim06: phase-phase

sim01 differences:
    * common ref. average (with mne)
    * alpha band filter (8 - 13 Hz)

'Juan Felipe Restrepo <juan.restrepo@uner.edu.ar>'
2022-02-21
===============================================================================
===============================================================================
[SimConfig]
Sim_filename='PP06s'
Sim_variables={'C':[1,2]}
Sim_realizations={'SUBJECT':10}
Sim_name='PP06s'
Sim_hostname='cluster-fiuner'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=debug
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
"""
import itertools
import os
import random
import socket
import sys
from datetime import datetime

import mne
import numpy as np
import pandas as pd
import psutil
import ray
from knnIMpy.knnTent import knnTent01 as tf
from matplotlib import pyplot as plt
from scipy.io import loadmat, savemat
from scipy.signal import hilbert

# from knnIMpy.knnTools import getAlpha

try:
    sys.path.append(
        os.path.join(os.path.dirname(__file__), "../../../../", "eegCTools")
    )
    import eegCTools as eegT
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), "../../../", "eegCTools"))
    import eegCTools as eegT


# if not ray.is_initialized():
#     num_cpus = psutil.cpu_count(logical=False)
#     ray.init(num_cpus=num_cpus)

ray.shutdown()
num_cpus = 24
ray.init(num_cpus=num_cpus)


@ray.remote
def calc_tent(indSorce, indTarget, data, dataS, tf_opt):
    return tf(dataS[indSorce, :], data[indTarget, :], **tf_opt)


# Simulation parameters
C = 1
SUBJECT = 1
NSUR = 512
# ----------------------------------------------------------------------------
# Change random seed
random.seed()
seed = random.randint(1, 5000)
np.random.seed(seed)
print("seed = {0}".format(seed), flush=True)

# output file
path = os.getcwd()
OUTDIR = path + "/outData/"
if not os.path.isdir(OUTDIR):
    os.makedirs(OUTDIR)
    print("created folder : ", OUTDIR, flush=True)
else:
    print(OUTDIR, "folder already exists.", flush=True)
oname = "PP06s_C0{0}".format(C)

# host settings
hostname = socket.gethostname()
if hostname == "apolo":
    # Apolo
    dataPath = "/media/jrestrepo/Datos/eegControl/GrupoControl/mat/"
elif hostname == "zeus":
    # zeus
    dataPath = "/home/jrestrepo/Datos/eegControl/GrupoControl/mat/"
elif hostname == "jupiter":
    # workstation
    dataPath = "/home/jrestrepo/eegcontrol/Datos/mat/"
else:
    # neptuno
    dataPath = "/home/jrestrepo/eegcontrol/Datos/mat/"
sim_opt = {}
sim = {}
sim_opt["seed"] = seed
sim_opt["hostname"] = hostname
# ----------------------------------------------------------------------------
# Simulation
now = datetime.now()
dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")
sim_opt["simDate"] = dt_string_ini
print("------------------------", flush=True)
print("simulation init  " + dt_string_ini, flush=True)
print("------------------------", flush=True)
# ----------------------------------------------------------------------------
# Data
if C == 1:
    condition = "OC"
    dataPath = dataPath + "VigiliaOjosCerrados/"
else:
    condition = "OA"
    dataPath = dataPath + "VigiliaOjosAbiertos/"

if SUBJECT < 10:
    dataFile = dataPath + "ID_0{0}_TF_{1}.mat".format(SUBJECT, condition)
    oname = oname + "_SJ0{0}.mat".format(SUBJECT)
else:
    dataFile = dataPath + "ID_{0}_TF_{1}.mat".format(SUBJECT, condition)
    oname = oname + "_SJ{0}.mat".format(SUBJECT)

data = loadmat(dataFile)
eeg = data["eeg"]
n0, nch = eeg.shape
fs = data["fs"]

# ----------------------------------------------------------------------------
# Crate mne object
ch_types = ["eeg"] * nch
ch_names = data["chanlocs"].tolist()
info = mne.create_info(ch_names, ch_types=ch_types, sfreq=fs)
eeg = mne.io.RawArray(data["eeg"].T, info)
eeg.set_montage(mne.channels.make_standard_montage("standard_1020"))

sim_opt["subject"] = SUBJECT
sim_opt["eegInfo"] = data["eegInfo"]
# sim_opt["mneInfo"] = eeg.info
sim_opt["refInfo"] = data["refInfo"]
sim_opt["condition"] = data["condition"]
sim_opt["chanlocs"] = data["chanlocs"]
sim_opt["fname"] = data["fname"]
sim_opt["fs"] = fs
sim_opt["fsUnits"] = "Hz"
sim_opt["nch"] = nch
sim_opt["OriginaldataLength"] = n0
sim_opt["nsur"] = NSUR

del data
# ----------------------------------------------------------------------------
# Set Referrence
# common ref. average
eeg.set_eeg_reference(ref_channels="average")
sim_opt["ref"] = "CommonRefAvg"

# # REST reference
# sphere = mne.make_sphere_model("auto", "auto", raw_eeg.info)
# src = mne.setup_volume_source_space(sphere=sphere, exclude=30.0, pos=5.0)
# forward = mne.make_forward_solution(raw_eeg.info, trans=None, src=src, bem=sphere)
# raw_rest = raw_eeg.copy().set_eeg_reference("REST", forward=forward)

# ----------------------------------------------------------------------------
# Filter EEG Bands

# Alpha filter
l_freq = 8
h_freq = 13
method = "iir"
eeg.filter(l_freq, h_freq, method=method)
sim_opt["FilterType"] = method
sim_opt["Lfreq"] = l_freq
sim_opt["Hfreq"] = h_freq
sim_opt["fband"] = "alpha"

# test filter
# x = eeg._data[0, :]
# f = np.linspace(-0.5, 0.5, x.size) * fs
# A = np.abs(scipy.fft.fftshift(scipy.fft.fft(x)))
# plt.plot(f.T, A)
# plt.show()


# ----------------------------------------------------------------------------
# Get phase
eeg = eeg._data
phase = np.angle(hilbert(eeg, axis=1))

# Split phase in windows
winFile = dataPath + "GrupoControlVentanas_30s_{0}.csv".format(condition)
df = pd.read_csv(winFile, sep=",")
_, w11, w12, w21, w22, _ = df.loc[SUBJECT - 1].values
n = w12 - w11
phasew = np.zeros((2, nch, n))
phasew[0, :, :] = phase[:, w11:w12]
phasew[1, :, :] = phase[:, w21:w22]
sim_opt["windowsLength"] = n
sim_opt["windowsNumber"] = 2

del eeg
del phase
# ----------------------------------------------------------------------------
# Transfer entropy parameters
m = 3
tau = 9
u = tau
kNearestN = 8
# alpha = getAlpha(kNearestN, 3 * m)
snorm = False
vcor = False
sim_opt["m"] = m
sim_opt["tau"] = tau
sim_opt["u"] = u
sim_opt["kNearestN"] = kNearestN
# sim_opt["alpha"] = alpha
sim_opt["snorm"] = snorm
sim_opt["vcor"] = vcor
# print("alpha done", flush=True)
# ----------------------------------------------------------------------------
# Transfer entropy
# set algorithm parameters
tf_opt = {
    "m": m,
    "tau": tau,
    "u": u,
    "nn": kNearestN,
    # "alpha": alpha,
    "snorm": snorm,
    "vcor": vcor,
}

# channel indices
i = []
j = []
for ch_inds in itertools.combinations(range(nch), 2):
    i.append(ch_inds[0])
    j.append(ch_inds[1])
ch_inds = (i, j)
nrea = int(nch * (nch - 1) / 2)

sim_opt["chinds"] = ch_inds
sim_opt["nrea"] = nrea
# ----------------------------------------------------------------------------

txys = np.zeros((2, nrea, NSUR))
tyxs = np.zeros((2, nrea, NSUR))

for r in range(NSUR):
    now = datetime.now()
    local_dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")

    phasewS = eegT.surrogateData(phasew, surtype="ranked02")

    for w in range(2):
        phase_id = ray.put(np.squeeze(phasew[w, :, :]))
        phaseS_id = ray.put(np.squeeze(phasewS[w, :, :]))
        Txys_ray_ids = []
        Tyxs_ray_ids = []
        for i, j in zip(*ch_inds):
            Txys_ray_ids.append(calc_tent.remote(i, j, phase_id, phaseS_id, tf_opt))
            Tyxs_ray_ids.append(calc_tent.remote(j, i, phase_id, phaseS_id, tf_opt))
        txys[w, :, r] = np.array(ray.get(Txys_ray_ids))
        tyxs[w, :, r] = np.array(ray.get(Tyxs_ray_ids))

    print("{0:.2f} %".format((r + 1) / NSUR * 100), flush=True)

sim["data"] = {"txys": txys, "tyxs": tyxs}
sim["opt"] = sim_opt
savemat(OUTDIR + oname, sim)
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print(dt_string, flush=True)

print("------------------------")
print("Sim start : ", dt_string_ini)
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("Sim end: ", dt_string)
print("------------------------")
