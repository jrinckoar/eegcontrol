%{
Análisis de datos para simulación 7
Fase-Fase 07

Preprocesamiento:
* Implementado common reference average
* Filtro en banda theta 4 - 8 Hz
* fase obtenida a partir de señal analítica y transformada de hilbert.

Parámetros Entropía de Transferencia:
* m = 4, tau = 7, u = m + 1, kNearestN = 8, snorm = False, vcor = False.

Surrogados:
* Se calcula surrogados para las series normales circular shift: Se mueven
todas las series juntas (no rompe causalidad).
* ranked pero de la serie de fase.

Análisis estadístico:
* Se promedian los datos de las 2 ventanas.
* Test de doble cola alfa=0.01

Información:
* Las matrice Tfourier y Tranked tienen dim (nch, nch, ncond, nsuj, nwin)
  Contienen las estimaciones de entropía de transfeencia Txy - Tyx.
    nch: número de canales 20.
    ncond: número de condiciones OC:1 y OA=2.
    nsuj: Número de sujetos 10.
    nwin: Número de ventanas analizadas.
* Las matrice Ffourier y Franked tienen dim (nch, nch, ncond, nsuj, nwin)
  Contienen el flujo de información: 1 si Txy > Tyx, -1 si Tyx > Txy y
  0 si Txy = Tyx.
%}
clear;
clc;
addpath('../../eegCTools/')
dataPath = '/media/jrestrepo/Datos/eegControl/sims/sim07/CompiledData/';
simN = 'sim07 M4 - knn8 - Theta band';
nch = 20;
alpha = 0.01;

% Cargar datos
file = [dataPath, 'PP07_CompiledData.mat'];
load(file)
chInds = opt.chinds + 1;
chnames = opt.chanlocs;

% Ojos Cerrados
[OC_tent, OC_flow]=statAnalysis06(OC.txy, OC.tyx, OC.txys, OC.tyxs, alpha);
OC_tent = vecToMat02(OC_tent, chInds);
OC_flow = vecToMat02(OC_flow, chInds);
% Ojos Abiertos
[OA_tent, OA_flow]=statAnalysis06(OA.txy, OA.tyx, OA.txys, OA.tyxs, alpha);
OA_tent = vecToMat02(OA_tent, chInds);
OA_flow = vecToMat02(OA_flow, chInds);

%% Graficar por sujeto
SUJETO=2;
% OCd = squeeze(OC_tent(SUJETO,:,:,:));
% OAd = squeeze(OA_tent(SUJETO,:,:,:));
OCd = squeeze(OC_flow(SUJETO,:,:,:));
OAd = squeeze(OA_flow(SUJETO,:,:,:));

blimit = min(min(OCd(:)), min(OAd(:)));
tlimit = max(max(OCd(:)), max(OAd(:)));
figure(1);
subplot(121)
plotResultsMatrix(squeeze(OCd(1,:,:)), blimit, tlimit, chnames)
title('Ventana 1')
subplot(122)
plotResultsMatrix(squeeze(OCd(2,:,:)), blimit, tlimit, chnames)
title('Ventana 2')
sgtitle([simN, ' - Ojos Cerrados - Txy - Tyx', ' - Sujeto ' num2str(SUJETO)])
figure(2);
subplot(121)
plotResultsMatrix(squeeze(OAd(1,:,:)), blimit, tlimit, chnames)
title('Ventana 1')
subplot(122)
plotResultsMatrix(squeeze(OAd(2,:,:)), blimit, tlimit, chnames)
title('Ventana 2')
sgtitle([simN, ' - Ojos Abiertos - Txy - Tyx', ' - Sujeto ' num2str(SUJETO)])

%% Graficar por promedio entre sujetos
% OCd = squeeze(OC_tent(SUJETO,:,:,:));
% OAd = squeeze(OA_tent(SUJETO,:,:,:));
OCd = squeeze(sum(OC_flow,1));
OAd = squeeze(sum(OA_flow,1));

blimit = min(min(OCd(:)), min(OAd(:)));
tlimit = max(max(OCd(:)), max(OAd(:)));

OCd(abs(OCd)<=1) = 0;
OAd(abs(OAd)<=1) = 0;

figure(1);
subplot(121)
plotResultsMatrix(squeeze(OCd(1,:,:)), blimit, tlimit, chnames)
title('Ventana 1')
subplot(122)
plotResultsMatrix(squeeze(OCd(2,:,:)), blimit, tlimit, chnames)
title('Ventana 2')
sgtitle([simN, ' - Ojos Cerrados - Txy - Tyx'])
figure(2);
subplot(121)
plotResultsMatrix(squeeze(OAd(1,:,:)), blimit, tlimit, chnames)
title('Ventana 1')
subplot(122)
plotResultsMatrix(squeeze(OAd(2,:,:)), blimit, tlimit, chnames)
title('Ventana 2')
sgtitle([simN, ' - Ojos Abiertos - Txy - Tyx'])
%% Graficar por promedio entre sujetos y ventanas
% OCd = squeeze(OC_tent(SUJETO,:,:,:));
% OAd = squeeze(OA_tent(SUJETO,:,:,:));
%OCd = squeeze(mean(mean(OC_tent,1),2));
%OAd = squeeze(mean(mean(OA_tent,1),2));
OCd = squeeze(sum(sum(OC_flow,1),2));
OAd = squeeze(sum(sum(OA_flow,1),2));

blimit = min(min(OCd(:)), min(OAd(:)));
tlimit = max(max(OCd(:)), max(OAd(:)));

OCd(abs(OCd)<=2) = 0;
OAd(abs(OAd)<=2) = 0;

figure();
subplot(121)
plotResultsMatrix(squeeze(OCd), blimit, tlimit, chnames)
title('Ojos Cerrados')
subplot(122)
plotResultsMatrix(squeeze(OAd), blimit, tlimit, chnames)
title('Ojos Abiertos')
sgtitle([simN, ' - Txy - Tyx'])

















%%


%     if ST == 1
%         Tfourier = vecToMat(Tent, chInds);
%         Ffourier = vecToMat(Flow, chInds);
%     else
%         Tranked = vecToMat(Tent, chInds);
%         Franked = vecToMat(Flow, chInds);
%     end
% end

%% Graficar flujo por sujeto

SUJETO = 6;
% Si se quiere graficar Entropía de Transferencia
Mf = squeeze(Tfourier(:,:,:,SUJETO));
Mr = squeeze(Tranked(:,:,:,SUJETO));

%Mf = squeeze(Ffourier(:,:,:,SUJETO));
%Mr = squeeze(Franked(:,:,:,SUJETO));

blimit = min(min(Mf(:)), min(Mr(:)));
tlimit = max(max(Mf(:)), max(Mr(:)));

% plot fourier surrogates
figure;
subplot(121)
plotResultsMatrix(Mf(:,:,1), blimit, tlimit, chnames)
title('Fourier eyes close')
subplot(122)
plotResultsMatrix(Mf(:,:,2), blimit, tlimit, chnames)
title('Fourier eyes open')
sgtitle([simN, 'Txy - Tyx', ' - Sujeto ' num2str(SUJETO)])
% plot ranked surrogates
figure;
subplot(121)
plotResultsMatrix(Mr(:,:,1), blimit, tlimit, chnames)
title('Ranked eyes close')
subplot(122)
plotResultsMatrix(Mr(:,:,2), blimit, tlimit, chnames)
title('Ranked eyes open')
sgtitle([simN, 'Txy - Tyx', ' - Sujeto ' num2str(SUJETO)])

%% Graficar Flujo entre sujetos
% Si se quiere graficar Entropía de Transferencia
% Mf = squeeze(Tfourier(:,:,:,SUJETO));
% Mr = squeeze(Tranked(:,:,:,SUJETO));

%Mf = squeeze(mean(Tfourier, 4));            % promediar por sujetos (Tent)
%Mr = squeeze(mean(Tranked, 4));             % promediar por sujetos

Mf = squeeze(sum(Ffourier, 4));             % sumar todos los sujetos (Flujo)
Mr = squeeze(sum(Franked, 4));              % sumar todos los sujetos

blimit = min(min(Mf(:)), min(Mr(:)));
tlimit = max(max(Mf(:)), max(Mr(:)));

% plot fourier surrogates
figure;
subplot(121)
plotResultsMatrix(Mf(:,:,1), blimit, tlimit, chnames)
title('Fourier eyes close')
subplot(122)
plotResultsMatrix(Mf(:,:,2), blimit, tlimit, chnames)
title('Fourier eyes open')
sgtitle([simN, 'Txy - Tyx'])
% plot ranked surrogates
figure;
subplot(121)
plotResultsMatrix(Mr(:,:,1), blimit, tlimit, chnames)
title('Ranked eyes close')
subplot(122)
plotResultsMatrix(Mr(:,:,2), blimit, tlimit, chnames)
title('Ranked eyes open')
sgtitle([simN, 'Txy - Tyx'])
