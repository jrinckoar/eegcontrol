
path = '~/Datos/eeg_tent_simdata/emb_lag/';
fname1 = 'Sim01_C1_01.mat';
fname2 = 'Sim01_C2_01.mat';
dataC = load([path, fname1]);
dataE = load([path, fname2]);


tmax = dataC.sim_opt.lag.tmax;
nch = dataC.sim_opt.eeg.nch;
nsplits = dataC.sim_opt.eeg.nsplits;

phaseC = dataC.data.MI_phase;
ampC = dataC.data.MI_amp;
phase_ampC = dataC.data.MI_phase_amp;
phaseE = dataE.data.MI_phase;
ampE = dataE.data.MI_amp;
phase_ampE = dataE.data.MI_phase_amp;

t = linspace(0, tmax, tmax +1);
for i=1:nch
    for j=1:nch
        %plot(t, squeeze(phaseC(:,i,j,:)));
        plot(t, squeeze(phaseE(:,i,j,:)));
        %plot(t, squeeze(ampC(:,i,j,:)));
        %plot(t, squeeze(phase_ampC(:,i,j,:)));
        grid on;
        title(['ch ', num2str(i) , '   ch  ', num2str(j)])
        pause
    end
end