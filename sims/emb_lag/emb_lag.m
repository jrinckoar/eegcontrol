%{
---------------------------------------------------------------------------
                       SIM 01 - OBTAIN EMBEDDING LAG
---------------------------------------------------------------------------
Calculate the embedding lag for the eeg time series
phase and amplitude

'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-07-23
===========================================================================
===========================================================================
[SimConfig]
Sim_filename='Sim01'
Sim_variables={'C':[1,2]}
Sim_realizations={'SREA':1}
Sim_name='S01'
Sim_hostname='cluster-fiuner'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
%}

%%.........................................................................
clear;
clc;

% Sim variables
SREA=1;
C=2;

% Output File
opath = [pwd, '/../data_out_lag/'];
oname = ['Sim01_C', num2str(C), '_0', num2str(SREA)];
ofile = [opath, oname, '.mat'];
if ~exist(opath, 'dir')
    mkdir(opath)
end

% execution time - init
[~,t_0]=unix('date "+%d-%H:%M:%S"');

% Change random seed
[~,sd] = unix('echo $RANDOM');
sd = str2double(sd);
rng(sd);

% Host settings
[~, hostname] = unix('echo $HOSTNAME');
disp(hostname)
if strcmp(hostname(1:5),'apolo')
% [Apolo]
    % Directorios
    addpath '/home/jrestrepo/Dropbox/inv/codes/knn_information_measures/';
    addpath '/home/jrestrepo/Dropbox/inv/eeg_tent/eeg_tent_tools/';
    data_path ='/home/jrestrepo/Dropbox/inv/eeg_tent/eeg_tent_tools/';
    % parpool
    pc = parcluster('local');
else
% [Neptuno]
    % Directorios
    addpath '/home/jrestrepo/codes/knn_information_measures/';
    addpath '/home/jrestrepo/eeg_tent/eeg_tent_tools/';
    data_path ='/home/jrestrepo/eeg_tent/eeg_tent_tools/';
    % parpool
    pc = parcluster('local');
    [~, slurm_cpu] = unix('echo $SLURM_CPUS_ON_NODE');
    pc.JobStorageLocation = ['home/jrestrepo/.matlab/temp/', slurm_cpu];
    parpool(pc, slurm_cpu);
end

sim_opt.seed = sd;
sim_opt.host = hostname;
%%.........................................................................
% Data
switch C
    case 1
        data_file = [data_path, 'EEGraw_Control_1.mat'];
        n = 37500;
        nsplits = 3;
    case 2
        data_file = [data_path, 'EEGraw_EQZ_1.mat'];
        n = 35072;
        nsplits = 3;
end
rawData = load(data_file);
Fs = rawData.EEG.sf;
channels_info = rawData.EEG.ChanName;
[eeg, ch_groups, fs] = zone_channels(rawData.EEG.Data, channels_info, Fs);


sim_opt.eeg.Fs = Fs;
sim_opt.eeg.fs_down = fs;
sim_opt.eeg.ch_groups = ch_groups;
clear rawData;
%%.........................................................................
% Time-Freq analysis
nch = length(ch_groups);
EEG_phase = cell(nch, nsplits);
EEG_amp = cell(nch, nsplits);
for i=1:nch
    eeg_hilbert = eeg(:,i) + 1j*hilbert(eeg(:,i));
    for j=1:nsplits
        EEG_phase{i, j} = angle(eeg_hilbert((j-1)*n + 1: j*n));
        EEG_amp{i, j} = abs(eeg_hilbert((j-1)*n + 1: j*n));
    end
end


sim_opt.eeg.nsplits = nsplits;
sim_opt.eeg.n = n;
clear eeg;
clear eeg_hilbert;
%%.........................................................................
% Embedding lag parameters
tmax = 50;
kNearestN = 15;
alpha = get_alpha(kNearestN, 2);

sim_opt.lag.tmax = tmax;
sim_opt.lag.kNearestN = kNearestN;
sim_opt.lag.alpha = alpha;
%%.........................................................................
% Get Transfer entorpy
fprintf('\n\n');
disp(oname)

MI_phase = zeros(tmax + 1, nch, nch, nsplits);
MI_amp = zeros(tmax +1, nch, nch, nsplits);
MI_phase_amp = zeros(tmax+1, nch, nch, nsplits);


fprintf('Progress:\n');
count = 0;
for i =1: nch
    for j=1: nch
        for k=1: nsplits
            MI_phase(:,i,j,k) = lag_mutualInfo(EEG_phase{i,k}, ...
                EEG_phase{j,k}, tmax, kNearestN, alpha);
            MI_amp(:,i,j,k) = lag_mutualInfo(EEG_amp{i,k}, ...
                EEG_amp{j,k}, tmax, kNearestN, alpha);
            MI_phase_amp(:,i,j,k) = lag_mutualInfo(EEG_phase{i,k}, ...
                EEG_amp{j,k}, tmax, kNearestN, alpha);
        end
    count = count + 1;
    disp([num2str((100*count)/(nch*nch)),' %']);
    end
end
data.MI_phase_amp = MI_phase_amp;
data.MI_phase = MI_phase;
data.MI_amp = MI_amp;
%%.........................................................................
% save results
save(ofile,'sim_opt', 'data');
% execution time - end
[~,t_f]=unix('date "+%d-%H:%M:%S"');
disp(['Execution: init ', t_0, '           End    ', t_f]);
disp('@@@ JOB DONE @@@');
