#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
EEG Control - transfer entropy

sim03: phase-phase

sim01 differences:
    * knnNeigthbors = 16


'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-11-24
===============================================================================
===============================================================================
[SimConfig]
Sim_filename='PP03'
Sim_variables={'C':[1,2]}
Sim_realizations={'SUBJECT':10}
Sim_name='PP03'
Sim_hostname='cluster-fiuner'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
"""
import itertools
import os
import random
import socket
import sys
from datetime import datetime

import numpy as np
import pandas as pd
import psutil
import ray
from knnIMpy.knnTent import knnTent01 as tf
# from knnIMpy.knnTools import getAlpha
from matplotlib import pyplot as plt
from scipy.io import loadmat, savemat
from scipy.signal import hilbert

try:
    sys.path.append(os.path.join(os.path.dirname(__file__), "../../../", "eegCTools"))
    import eegCTools as eegT
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), "../../", "eegCTools"))
    import eegCTools as eegT


if not ray.is_initialized():
    num_cpus = psutil.cpu_count(logical=False)
    ray.init(num_cpus=num_cpus)


@ray.remote
def calc_tent(indSorce, indTarget, data, tf_opt):
    return tf(data[:, indSorce], data[:, indTarget], **tf_opt)


# Simulation parameters
C = 1
SUBJECT = 1
# ----------------------------------------------------------------------------
# Change random seed
random.seed()
seed = random.randint(1, 5000)
np.random.seed(seed)
print("seed = {0}".format(seed), flush=True)

# output file
path = os.getcwd()
OUTDIR = path + "/outData/"
if not os.path.isdir(OUTDIR):
    os.makedirs(OUTDIR)
    print("created folder : ", OUTDIR, flush=True)
else:
    print(OUTDIR, "folder already exists.", flush=True)
oname = "PP03_C0{0}".format(C)

# host settings
hostname = socket.gethostname()
if hostname == "apolo":
    # Apolo
    dataPath = "/home/jrestrepo/Datos/eegControl/GrupoControl/mat/"
else:
    # neptuno
    dataPath = "/home/jrestrepo/eegcontrol/Datos/mat/"
sim_opt = {}
sim = {}
sim_opt["seed"] = seed
sim_opt["hostName"] = hostname
# ----------------------------------------------------------------------------
# Simulation
now = datetime.now()
dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")
sim_opt["simDate"] = dt_string_ini
print("------------------------", flush=True)
print("simulation init  " + dt_string_ini, flush=True)
print("------------------------", flush=True)
# ----------------------------------------------------------------------------
# Data
if C == 1:
    condition = "OC"
    dataPath = dataPath + "VigiliaOjosCerrados/"
else:
    condition = "OA"
    dataPath = dataPath + "VigiliaOjosAbiertos/"

if SUBJECT < 10:
    dataFile = dataPath + "ID_0{0}_TF_{1}.mat".format(SUBJECT, condition)
    oname = oname + "_SJ0{0}.mat".format(SUBJECT)
else:
    dataFile = dataPath + "ID_{0}_TF_{1}.mat".format(SUBJECT, condition)
    oname = oname + "_SJ{0}.mat".format(SUBJECT)

data = loadmat(dataFile)
eeg = data["eeg"]
n0, nch = eeg.shape

# common ref average ? hacer o no hacer?
# eeg = eeg - eeg.mean(axis=1, keepdims=True)

# channel indices
i = []
j = []
for ch_inds in itertools.combinations(range(nch), 2):
    i.append(ch_inds[0])
    j.append(ch_inds[1])
ch_inds = (i, j)
nrea = int(nch * (nch - 1) / 2)

sim_opt["subject"] = SUBJECT
sim_opt["eegInfo"] = data["eegInfo"]
sim_opt["refInfo"] = data["refInfo"]
sim_opt["condition"] = data["condition"]
sim_opt["chanlocs"] = data["chanlocs"]
sim_opt["fname"] = data["fname"]
sim_opt["fs"] = data["fs"]
sim_opt["fsUnits"] = "Hz"
sim_opt["nch"] = nch
sim_opt["OriginaldataLength"] = n0
sim_opt["chinds"] = ch_inds
sim_opt["nrea"] = nrea
del data
# ----------------------------------------------------------------------------
# Get phase
# Normalize zero mean unitary variance
eeg = (eeg - eeg.mean(axis=0, keepdims=True)) / eeg.std(axis=0, keepdims=True)
phase = np.angle(hilbert(eeg, axis=0))
print("phases done", flush=True)

# Split phase in windows
winFile = dataPath + "GrupoControlVentanas_30s_{0}.csv".format(condition)
df = pd.read_csv(winFile, sep=",")
_, w11, w12, w21, w22, _ = df.loc[SUBJECT - 1].values
n = w12 - w11
phasew = np.zeros((n, nch, 2))
phasew[:, :, 0] = phase[w11:w12, :]
phasew[:, :, 1] = phase[w21:w22, :]
sim_opt["windowsLength"] = n
sim_opt["windowsNumber"] = 2
del eeg
del phase
# ----------------------------------------------------------------------------
# Transfer entropy parameters
m = 4
tau = 3
u = 1
kNearestN = 16
# alpha = getAlpha(kNearestN, 3 * m)
snorm = False
vcor = False
sim_opt["m"] = m
sim_opt["tau"] = tau
sim_opt["u"] = u
sim_opt["kNearestN"] = kNearestN
# sim_opt["alpha"] = alpha
sim_opt["snorm"] = snorm
sim_opt["vcor"] = vcor
# print("alpha done", flush=True)
# ----------------------------------------------------------------------------
# Transfer entropy
# set algorithm parameters
tf_opt = {
    "m": m,
    "tau": tau,
    "u": u,
    "nn": kNearestN,
    # "alpha": alpha,
    "snorm": snorm,
    "vcor": vcor,
}

txy = np.zeros((nrea, 2))
tyx = np.zeros((nrea, 2))

for w in range(2):
    phase_id = ray.put(np.squeeze(phasew[:, :, w]))
    Txy_ray_ids = []
    Tyx_ray_ids = []
    for i, j in zip(*ch_inds):
        Txy_ray_ids.append(calc_tent.remote(i, j, phase_id, tf_opt))
        Tyx_ray_ids.append(calc_tent.remote(j, i, phase_id, tf_opt))
    txy[:, w] = np.array(ray.get(Txy_ray_ids))
    tyx[:, w] = np.array(ray.get(Tyx_ray_ids))

    print("{0:.2f} %".format(((w + 1) / 2) * 100, flush=True))


sim["data"] = {"txy": txy, "tyx": tyx}
sim["opt"] = sim_opt
savemat(OUTDIR + oname, sim)

print("------------------------", flush=True)
print("Sim start : ", dt_string_ini, flush=True)
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("Sim end: ", dt_string, flush=True)
print("------------------------", flush=True)
