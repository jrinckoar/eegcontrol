%{
---------------------------------------------------------------------------
                   SIM 01 - TRANSFER ENTROPY PHASE-PHASE
---------------------------------------------------------------------------
Calculate the transfer entropy between eeg phases

'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-10-18
===========================================================================
===========================================================================
[SimConfig]
Sim_filename='EZS01'
Sim_variables={'C':[1,2], 'M':[3,6]}
Sim_realizations={'SREA':1}
Sim_name='EZS01'
Sim_hostname='cluster-fiuner'
Sim_MatExecutable='matlab2018'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
%}

%%.........................................................................
clear;
clc;

% Sim variables
SREA=1;
M=4;
C=1;
NSUR=30;

% Output File
opath = [pwd, '/../dataOut/'];
oname = ['PP01_C', num2str(C), '_M', num2str(M), '_0', num2str(SREA)];
ofile = [opath, oname, '.mat'];
if ~exist(opath, 'dir')
    mkdir(opath)
end

% execution time - init
[~,t_0]=unix('date "+%d-%H:%M:%S"');

% Change random seed
[~,sd] = unix('echo $RANDOM');
sd = str2double(sd);
rng(sd);

% Host settings
[~, hostname] = unix('echo $HOSTNAME');
disp(hostname)
if strcmp(hostname(1:end-1),'apolo')
% [Apolo]
    % Directorios
    addpath '/home/jrestrepo/Dropbox/inv/codes/knn-information-measures/matlab/knnIMmat/';
    addpath '/home/jrestrepo/Dropbox/inv/eegEzq/eegEzqTools/';
    data_path ='/home/jrestrepo/Dropbox/inv/eegEzq/eegEzqTools/';
    % parpool
    pc = parcluster('local');
else
% [Neptuno]
    % Directorios
    addpath '/home/jrestrepo/codes/knn-information-measures/matlab/knnIMmat/';
    addpath '/home/jrestrepo/eegEzq/eegEzqTools/';
    data_path ='/home/jrestrepo/eegEzq/eegEzqTools/';
    % parpool
    pc = parcluster('local');
    % [~, slurm_cpu] = unix('echo $SLURM_CPUS_ON_NODE');
    % pc.JobStorageLocation = ['home/jrestrepo/.matlab/temp/', slurm_cpu];
    slurm_cpu=23;
    ps = parallel.Settings;
    ps.Pool.AutoCreate = false; %do not autocreate parpool when encountering a |parfor|
    ps.Pool.IdleTimeout = Inf;  %do not shutdown parpool after Inf idle time
    parpool(pc, slurm_cpu);
end

sim_opt.seed = sd;
sim_opt.host = hostname;
%% Data ....................................................................
switch C
    case 1
        data_file = [data_path, 'EEGraw_Control_1.mat'];
        % n = 37500;
        % nsplits = 3;
    case 2
        data_file = [data_path, 'EEGraw_EQZ_1.mat'];
        % n = 35072;
        % nsplits = 3;
end
Data = load(data_file);
Fs = Data.EEG.sf;
chNames = Data.EEG.ChanName;
nch = length(chNames);
% Cut data length
dataLength = 210000;                        % 14 minutes
EEGr = Data.EEG.Data(1:dataLength,:);

% Resample to Fs = 100 Hz
EEGr = resample(EEGr, 2, 5,'Dimension', 1);
fs = Fs * 2 / 5;

% Normalize to cero mean - unitary variance
EEGr = (EEGr - mean(EEGr, 1)) ./ std(EEGr, [], 1);

sim_opt.eeg.origin_Fs = Fs;
sim_opt.eeg.dataLength = dataLength;
sim_opt.eeg.fs = fs;
sim_opt.eeg.nch = nch;
sim_opt.eeg.chNames = chNames;
clear Data;
%% Get phases ..............................................................
% analytic signal and phases
EEGr = angle(hilbert(EEGr));

% Split in temporal windows
nsplits = 14;
n = 6000;
EEGp = zeros(nsplits, nch, n);
for i=1:nsplits
    EEGp(i,:,:) = EEGr((i-1)*n + 1:i*n, :)';
end

sim_opt.eeg.nsplits = nsplits;
sim_opt.eeg.n = n;
clear EEGr;
%%.........................................................................
% Embedding lag parameters
m = M;
tau = 9;
kNearestN = 8;
u = m + [1, 5, 10];
alpha = getAlpha(kNearestN, 3*m);
%alpha = 0.2;
nSur = NSUR;
snorm = 0;

sim_opt.tent.m = m;
sim_opt.tent.tau = tau;
sim_opt.tent.kNearestN = kNearestN;
sim_opt.tent.alpha = alpha;
sim_opt.tent.u = u;
sim_opt.tent.snorm = snorm;
sim_opt.tent.nSur = nSur;
%%.........................................................................
% Get Transfer entorpy
fprintf('\n\n');
disp(oname)

U = length(u);
rea = nch * (nch - 1) / 2;
chInds = nchoosek(1:nch,2);
sourceInds = chInds(:,1);
targetInds = chInds(:,2);
sim_opt.chInds = chInds;
sim_opt.rea = rea;

txy = zeros(nsplits, rea, U);
tyx = zeros(nsplits, rea, U);

fprintf('Progress:\n');
count = 0;
for i=1: nsplits
    eegp = squeeze(EEGp(i, :, :));
    for j=1:U
        uk = u(j);
        for r=1:rea
            xt = eegp(sourceInds(r),:);
            yt = eegp(targetInds(r),:);
            txy(i, r, j) = knnTent01(xt, yt, m, tau, uk, ...
                kNearestN, alpha, snorm);
            tyx(i, r, j) = knnTent01(yt, xt, m, tau, uk, ...
                kNearestN, alpha, snorm);
        end
        count = count + 1;
        disp([num2str((100 * count) / (nsplits * U)), ' %']);
    end
end
data.txy = txy;
data.tyx = tyx;
%%.........................................................................
% save results
save(ofile,'sim_opt', 'data');
% execution time - end
[~,t_f]=unix('date "+%d-%H:%M:%S"');
disp(['Execution: init ', t_0, '           End    ', t_f]);
disp('@@@ JOB DONE @@@');
