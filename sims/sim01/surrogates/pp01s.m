%{
---------------------------------------------------------------------------
             SIM 01 - TRANSFER ENTROPY PHASE-PHASE - SURROGATE
---------------------------------------------------------------------------
Calculate the trnasfer entropy between eeg phases

'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-08-11
===========================================================================
===========================================================================
[SimConfig]
Sim_filename='PP01s'
Sim_variables={'C':[1,2], 'M':[4,8,10]}
Sim_realizations={'SREA':8}
Sim_name='PP01s'
Sim_hostname='cluster-fiuner'
Sim_MatExecutable='matlab2018'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
%}

%%.........................................................................
clear;
clc;

% Sim variables
SREA=1;
M=1;
C=1;

% Output File
opath = [pwd, '/../dataOutSur/'];
oname_pre = ['PP01s_C', num2str(C), '_M', num2str(M)];
if ~exist(opath, 'dir')
    mkdir(opath)
end

% execution time - init
[~,t_0]=unix('date "+%d-%H:%M:%S"');

% Change random seed
[~,sd] = unix('echo $RANDOM');
sd = str2double(sd);
rng(sd);

% Host settings
[~, hostname] = unix('echo $HOSTNAME');
disp(hostname)
if strcmp(hostname(1:end-1),'apolo')
% [Apolo]
    % Directorios
    addpath '/home/jrestrepo/Dropbox/inv/codes/knn_information_measures/';
    addpath '/home/jrestrepo/Dropbox/inv/eeg_tent_ezq/eeg_tent_tools/';
    data_path ='/home/jrestrepo/Dropbox/inv/eeg_tent_ezq/eeg_tent_tools/';
    % parpool
    pc = parcluster('local');
else
% [Neptuno]
    % Directorios
    addpath '/home/jrestrepo/codes/knn_information_measures/';
    addpath '/home/jrestrepo/eeg_tent_ezq/eeg_tent_tools/';
    data_path ='/home/jrestrepo/eeg_tent_ezq/eeg_tent_tools/';
    delete(gcp('nocreate')); %delete the current pool
    % parpool
    pc = parcluster('local');
    % [~, slurm_cpu] = unix('echo $SLURM_CPUS_ON_NODE');
    slurm_cpu=23;
    ps = parallel.Settings;
    ps.Pool.AutoCreate = false; %do not autocreate parpool when encountering a |parfor|
    ps.Pool.IdleTimeout = Inf;  %do not shutdown parpool after Inf idle time
    parpool(pc, slurm_cpu);
    % pc.JobStorageLocation = ['home/jrestrepo/.matlab/temp/', slurm_cpu];
    parpool(pc, slurm_cpu);
end

sim_opt.seed = sd;
sim_opt.host = hostname;
%%.........................................................................
% Data
switch C
    case 1
        data_file = [data_path, 'EEGraw_Control_1.mat'];
        n = 37500;
        nsplits = 3;
    case 2
        data_file = [data_path, 'EEGraw_EQZ_1.mat'];
        n = 35072;
        nsplits = 3;
end
rawData = load(data_file);
Fs = rawData.EEG.sf;
channels_info = rawData.EEG.ChanName;
[eeg, ch_groups, fs] = zone_channels(rawData.EEG.Data, channels_info, Fs);

x = rawData.EEG.Data;
sim_opt.eeg.Fs = Fs;
sim_opt.eeg.fs_down = fs;
sim_opt.eeg.ch_groups = ch_groups;
clear rawData;
clear surEEG;
%%.........................................................................
% Time-Freq analysis
nch = length(ch_groups);
EEG_phase = cell(nch, nsplits);
EEG_phaseSur = cell(nch, nsplits);
% EEG_amp = cell(nch, nsplits);

% eeg phases
for i=1:nch
    eeg_hilbert = eeg(:,i) + 1j*hilbert(eeg(:,i));
    for j=1:nsplits
        EEG_phase{i, j} = angle(eeg_hilbert((j-1)*n + 1: j*n));
    end
end

sim_opt.eeg.nsplits = nsplits;
sim_opt.eeg.n = n;
clear eeg;
clear eeg_hilbert;
%%.........................................................................
% Embedding lag parameters
m = M;
tau = 9;
kNearestN = 25;
u = [1, 5];
alpha = get_alpha(kNearestN, 3*m);
nSur = 8;
totalnSur=64;
U = length(u);

sim_opt.tent.m = m;
sim_opt.tent.tau = tau;
sim_opt.tent.kNearestN = kNearestN;
sim_opt.tent.alpha = alpha;
sim_opt.tent.u = u;
sim_opt.tent.nSur = nSur;
sim_opt.tent.totalnSur = totalnSur;
%%.........................................................................
% Get Transfer entorpy
fprintf('\n\nSurrogates:\n');
for ns=1:nSur
    % Generate surrogate data
    [t1,t2] = size(x);
    surEEG = zeros(t1, t2);
    for j=1:t2
        surEEG(:,j) = real(ifft(abs(fft(x(:,j))) .* exp(1j.*angle(fft(randn(t1,1))))));
    end
    [eegs, ~, ~] = zone_channels(surEEG, channels_info, Fs);
    % surrogates phases
    for i=1:nch
        eeg_hilbert = eegs(:,i) + 1j*hilbert(eegs(:,i));
        for j=1:nsplits
            EEG_phaseSur{i, j} = angle(eeg_hilbert((j-1)*n + 1: j*n));
        end
    end
    clear eegs;

    txys = zeros(nch, nch, U, nsplits);
    tyxs = zeros(nch, nch, U, nsplits);
    % outputfile
    fnumber = (SREA-1)*nSur + ns;
    if fnumber < 10
        oname = [oname_pre, '_0', num2str(fnumber)];
    else
        oname = [oname_pre, '_', num2str(fnumber)];
    end
    ofile = [opath, oname, '.mat'];
    disp(oname)
    [~,t_0s]=unix('date "+%d-%H:%M:%S"');
    fprintf('Progress:\n');
    count = 0;
    for k=1:nsplits
        for i =1:nch-1
            xp = EEG_phase{i,k};
            xs = EEG_phaseSur{i,k};
            for j= i+1:nch
                yp = EEG_phase{j,k};
                ys = EEG_phaseSur{j,k};
                for l=1:U
                    txys(i,j,l,k) = knn_tent02(xs, yp, m, tau,...
                        u(l), kNearestN, alpha);
                    tyxs(i,j,l,k) = knn_tent02(ys, xp, m, tau,...
                        u(l), kNearestN, alpha);
                    count = count + 1;
                end
            end
        end
        disp([num2str((100*count)/(U*nsplits*nch*(nch-1)/2)),' %']);
    end
    data.txys = txys;
    data.tyxs = tyxs;
    % save results
    save(ofile,'sim_opt', 'data');
    disp([oname, ' done'])
    [~,t_fs]=unix('date "+%d-%H:%M:%S"');
    disp(['Execution: init ', t_0s, '           End    ', t_fs]);
end
%%.........................................................................
% execution time - end
[~,t_f]=unix('date "+%d-%H:%M:%S"');
disp(['Execution: init ', t_0, '           End    ', t_f]);
disp('@@@ JOB DONE @@@');
