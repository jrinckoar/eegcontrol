# Simulaciones EEG Control

## Sim06

**nombre del archivo de simulación:** pp06.py

**parámetros de la simulación:**

* m = 4, tau = 3, u = 1, kNearestN = 8, snorm = False, vcor = False.

**Descripción:** Igual a la Sim05 pero los cálculos se hacen sobre el EGG
filtrado en la banda alfa (8 - 12 Hz)

**Preprocesamiento del EEG:**

* Remover media entre canales.
* Normalización de cada canal para tener media cero y varianza unitaria.
* Filtrar cada canal filtro?
* Transformada de Hilbert, calcular la fase y dividir en ventanas de 30 seg.

---

## Sim05

**nombre del archivo de simulación:** pp05.py

**parámetros de la simulación:**

* m = 4, tau = 3, u = 1, kNearestN = 8, snorm = False, vcor = False.

**Descripción:** Igual a la Sim01 pero se implementa una forma de obtener una
especie de distribución alrededor del cálculo de transferencia de entropía
original.

Esta distribución  del estadístico TENT  se obtiene haciendo  un desplazamiento
circular (circular shift) conjunto de los datos  de fase EEG (los datos en cada
ventana de 30 seg).  Desplazar conjuntamente circularmente todas las series por
la misma cantidad de datos, crea nuevas series que conservan la relación causal
entre ellas.  Para  cada nueva  serie se calcula  la entropía  de transferencia
obteniendo una distribución de la TENT bajo la condición de conexión causal.

La idea es poder comparar de forma estadística la distribución bajo la
condición de conexión causal con la distribución bajo la condición de no
conexión (obtenida con surrogados de Fourier o ranked).

---

## Sim04

**nombre del archivo de simulación:** pp04.py

**parámetros de la simulación:**

* m = 4, tau = 3, u = 1, kNearestN = 8, snorm = False, vcor = False.

**Descripción:** Igual a la Sim01 pero se implementa el common refrence
average: Se remueve de cada canal de EEG el promedio entre todos los canales.

---

## Sim03

**nombre del archivo de simulación:** pp03.py

**parámetros de la simulación:**

* m = 4, tau = 3, u = 1, kNearestN = 16, snorm = False, vcor = False.

**Descripción:** Igual a la Sim01 pero el parámetro kNearestN = 16.

---

## Sim02

**nombre del archivo de simulación:** pp02.py

**parámetros de la simulación:**

* m = 3, tau = 3, u = 1, kNearestN = 8, snorm = False, vcor = False.

**Descripción:** Igual a la Sim01 pero el parámetro m =3.

---

## Sim01

**nombre del archivo de simulación:** pp01.py

**parámetros de la simulación:**

* m = 4, tau = 3, u = 1, kNearestN = 8, snorm = False, vcor = False.

**Descripción:**
Calcula la entropía de transferencia  entre canales de EEG a partir de la fase
de la señal analítica de cada canal, obtenida mediante la transformada de
Hilbert.

Las señales de EEG fueron tomadas en 10 sujetos sanos (20 canales sistema
10/20) a una frecuencia de muestreo de 65 Hz con un equipo marca Neutronic.
Además la referencia es Bimastoidea, los canales F1-F3-C3-P3-O1-F7-T3-T5-Fz y
Oz referencian a A1 (Referencia izquierda) y el resto a A2.

Cada sujeto tiene registros bajo el estado de ojos cerrados (estado 1) y ojos
abiertos (estado 2). De cada registro se seleccionaron 2 ventanas de 30 seg,
los tiempos de inicio y fin de cada ventana están en los archivos:

  1. "GrupoControlVentanas_30s_OC.csv" (Ojos cerrados)
  2. "GrupoControlVentanas_30s_OA.csv" (Ojos abiertos)

**Preprocesamiento del EEG:** Normalización de cada canal para tener media cero
y varianza unitaria. Luego se aplica la transformada de Hilbert y se calcula la
fase, posteriormente, la fase de cada canal se divide en ventanas de 30 seg.

**Surrogados:** En esta simulación se realiza dos tipos de datos surrogados. El
primer tipo es basados en la transformada inversa de Fourier y el segundo
basado en el ordenamiento (ranking) de una realización de ruido blanco
Gaussiano. Se calcularon 512 realizaciones de cada uno.

* Surrogados basados en la transformada  de Fourier:  Se calcula el espectro de
amplitud de cada canal de EEG,  se multiplica  con el espectro de fase de ruido
blanco Gaussiano y se aplica la transformada inversa de Fourier
para obtener la señal surrogada:
```
    X = np.abs(fft(data, axis=0))
    # random phase
    randPhase = np.angle(fft(np.random.randn(*data.shape), axis=0))
    xs = np.real(ifft(X * np.exp(1j * randPhase), axis=0))
```

* Surrogados basados en el ordenamiento de ruido blanco Gaussiano: Se obtiene
una realización de ruido blanco Gaussiano del mismo tamaño de la señal de EEG,
luego se ordena de forma ascendente y se toma los índices temporales de cada
dato ordenado. Finalmente, se ordena la señal de EEG según los índices
temporales obtenidos.

Luego de obtener las series surrogadas, estas se normalizan (media cero y
varianza unitaria) y se calcula su fase (Hilbert).
