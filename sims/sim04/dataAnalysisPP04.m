%{
Análisis de datos para simulación 4
Fase-Fase 04

Preprocesamiento:
* Implementado common reference average
* eeg normalizado por canal, media cero y varianza unitaria.
* fase obtenida a partir de señal analítica y transformada de hilbert.

Parámetros Entropía de Transferencia:
* m = 4, tau = 3, u = 1, kNearestN = 8, snorm = False, vcor = False.

Surrogados:
* Fourier y ranked.

Análisis estadístico:
* Se promedian los datos de las 2 ventanas.
* Test de doble cola alfa=0.01

Información:
* Las matrice Tfourier y Tranked tienen dim (nch, nch, ncond, nsuj, nwin)
  Contienen las estimaciones de entropía de transfeencia Txy - Tyx.
    nch: número de canales 20.
    ncond: número de condiciones OC:1 y OA=2.
    nsuj: Número de sujetos 10.
    nwin: Número de ventanas analizadas.
* Las matrice Ffourier y Franked tienen dim (nch, nch, ncond, nsuj, nwin)
  Contienen el flujo de información: 1 si Txy > Tyx, -1 si Tyx > Txy y
  0 si Txy = Tyx.
%}
clear;
clc;
addpath('../../eegCTools/')
dataPath = '~/Datos/eegControl/sims/sim04/CompiledData/';
simN = 'sim03 M4 - knn8 common ref. average    ';
SURTYPE = {'Fourier','Ranked'};
nch = 20;
alpha = 0.01;

for ST=1:length(SURTYPE)
    file = [dataPath, 'PP04_', SURTYPE{ST},'_Data.mat'];
    load(file)
    chInds = simopt.chinds + 1;
    chnames = simopt.chanlocs;

    [Tent, Flow]=statAnalysis(txy, tyx, txys, tyxs, alpha);
    if ST == 1
        Tfourier = vecToMat(Tent, chInds);
        Ffourier = vecToMat(Flow, chInds);
    else
        Tranked = vecToMat(Tent, chInds);
        Franked = vecToMat(Flow, chInds);
    end
end

%% Graficar Flujo por sujeto

SUJETO = 1;
% Si se quiere graficar Entropía de Transferencia
Mf = squeeze(Tfourier(:,:,:,SUJETO));
Mr = squeeze(Tranked(:,:,:,SUJETO));

%Mf = squeeze(Ffourier(:,:,:,SUJETO));
%Mr = squeeze(Franked(:,:,:,SUJETO));

blimit = min(min(Mf(:)), min(Mr(:)));
tlimit = max(max(Mf(:)), max(Mr(:)));

% plot fourier surrogates
figure;
subplot(121)
plotResultsMatrix(Mf(:,:,1), blimit, tlimit, chnames)
title('Fourier eyes close')
subplot(122)
plotResultsMatrix(Mf(:,:,2), blimit, tlimit, chnames)
title('Fourier eyes open')
sgtitle([simN, 'Txy - Tyx', ' - Sujeto ' num2str(SUJETO)])
% plot ranked surrogates
figure;
subplot(121)
plotResultsMatrix(Mr(:,:,1), blimit, tlimit, chnames)
title('Ranked eyes close')
subplot(122)
plotResultsMatrix(Mr(:,:,2), blimit, tlimit, chnames)
title('Ranked eyes open')
sgtitle([simN, 'Txy - Tyx', ' - Sujeto ' num2str(SUJETO)])

%% Graficar Flujo entre sujetos
% Si se quiere graficar Entropía de Transferencia
% Mf = squeeze(Tfourier(:,:,:,SUJETO));
% Mr = squeeze(Tranked(:,:,:,SUJETO));

Mf = squeeze(mean(Tfourier, 4));            % promediar por sujetos (Tent)
Mr = squeeze(mean(Tranked, 4));             % promediar por sujetos

%Mf = squeeze(sum(Ffourier, 4));             % sumar todos los sujetos (Flujo)
%Mr = squeeze(sum(Franked, 4));              % sumar todos los sujetos

blimit = min(min(Mf(:)), min(Mr(:)));
tlimit = max(max(Mf(:)), max(Mr(:)));

% plot fourier surrogates
figure;
subplot(121)
plotResultsMatrix(Mf(:,:,1), blimit, tlimit, chnames)
title('Fourier eyes close')
subplot(122)
plotResultsMatrix(Mf(:,:,2), blimit, tlimit, chnames)
title('Fourier eyes open')
sgtitle([simN, 'Txy - Tyx'])
% plot ranked surrogates
figure;
subplot(121)
plotResultsMatrix(Mr(:,:,1), blimit, tlimit, chnames)
title('Ranked eyes close')
subplot(122)
plotResultsMatrix(Mr(:,:,2), blimit, tlimit, chnames)
title('Ranked eyes open')
sgtitle([simN, 'Txy - Tyx'])
