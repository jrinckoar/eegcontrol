%{
Análisis de datos para simulación 10
Fase-Fase 12

Preprocesamiento:
* Implementado common reference average
* Filtro en banda alfa 8 - 13 Hz
* fase obtenida a partir de señal analítica y transformada de hilbert.
* Señal completa sin ventaneo.

Parámetros Entropía de Transferencia:
* m = 4, tau = 7, u = m + 1, kNearestN = 8, snorm = False, vcor = False.

Surrogados:
* Se calcula surrogados para las series normales circular shift: Se mueven
todas las series juntas (no rompe causalidad).
* Surrogate fft based from eeg signals (just in surrogate file)

Análisis estadístico:
* Se promedian los datos de las 2 ventanas.
* Test de doble cola alfa=0.01

Información:
* Las matrice Tfourier y Tranked tienen dim (nsuj, nch, nch, ncond, nsuj, nwin)
  Contienen las estimaciones de entropía de transfeencia Txy - Tyx.
    nch: número de canales 20.
    ncond: número de condiciones OC:1 y OA=2.
    nsuj: Número de sujetos 10.
* Las matrice Ffourier y Franked tienen dim (nch, nch, ncond, nsuj, nwin)
  Contienen el flujo de información: 1 si Txy > Tyx, -1 si Tyx > Txy y
  0 si Txy = Tyx.
%}
clear;
clc;
addpath('../../eegCTools/')
addpath('../sim09/')
dataPath = '/media/jrestrepo/Datos/eegControl/sims/sim12/CompiledData/';
simN = 'sim12 M4 - knn8 - alpha band';
nch = 20;
alpha = 0.01;

% Cargar datos
file = [dataPath, 'PP12_CompiledData.mat'];
load(file)
chInds = opt.chinds + 1;
chnames = opt.chanlocs;

% Ojos Cerrados
[OC_tent, OC_flow, OC_coupling]=statAnalysis10(OC.txy, OC.tyx, OC.txys, OC.tyxs, alpha);
OC_tent = vecToMat09(OC_tent, chInds);
OC_flow = vecToMat09(OC_flow, chInds);
OC_coupling = vecToMat09(OC_coupling, chInds);

% Ojos Abiertos
[OA_tent, OA_flow, OA_coupling]=statAnalysis10(OA.txy, OA.tyx, OA.txys, OA.tyxs, alpha);
OA_tent = vecToMat09(OA_tent, chInds);
OA_flow = vecToMat09(OA_flow, chInds);
OA_coupling = vecToMat09(OA_coupling, chInds);

%% Graficar por sujeto
for i =1:10
    SUJETO=i;
     %OCd = squeeze(OC_tent(SUJETO,:,:,:));
     %OAd = squeeze(OA_tent(SUJETO,:,:,:));
    OCd = squeeze(OC_flow(SUJETO,:,:));
    OAd = squeeze(OA_flow(SUJETO,:,:));
    OCc = squeeze(OC_coupling(SUJETO,:,:));
    OAc = squeeze(OA_coupling(SUJETO,:,:));


    blimit = min(min(OCd(:)), min(OAd(:)));
    tlimit = max(max(OCd(:)), max(OAd(:)));


    figure(10);
    subplot(121)
    plotResultsMatrix(squeeze(OCd), blimit, tlimit, chnames)
    subplot(122)
    plotResultsMatrix(squeeze(OCc), 0, 3, chnames)
    %title('Ojos Cerrados')
    sgtitle([simN, ' - Txy - Tyx - Ojos Cerrados ', ' - Sujeto ' num2str(SUJETO)])
    %subplot(122)
    figure(11);
    subplot(121)
    plotResultsMatrix(squeeze(OAd), blimit, tlimit, chnames)
    subplot(122)
    plotResultsMatrix(squeeze(OAc), 0, 3, chnames)
    %title('Ojos Abiertos')
    sgtitle([simN, ' - Txy - Tyx Ojos Abiertos ', ' - Sujeto ' num2str(SUJETO)])
    pause
end
%% Graficar por promedio entre sujetos
OCd = squeeze(sum(OC_flow,1));
OAd = squeeze(sum(OA_flow,1));
%OCd = squeeze(mean(OC_tent,1));
%OAd = squeeze(mean(OA_tent,1));

blimit = min(min(OCd(:)), min(OAd(:)));
tlimit = max(max(OCd(:)), max(OAd(:)));

OCd(abs(OCd)<=3) = 0;
OAd(abs(OAd)<=3) = 0;

figure(10);
%subplot(121)
plotResultsMatrix(squeeze(OCd), blimit, tlimit, chnames)
title('Ojos Cerrados')
figure(11)
plotResultsMatrix(squeeze(OAd), blimit, tlimit, chnames)
title('Ojos Abiertos')
%sgtitle([simN, ' - Txy - Tyx'])
%% SAVE DATA 
OC = struct();
OA = struct();
OC.flow = OC_flow;
OC.tent = OC_tent;
OC.coupling = OC_coupling;
OA.flow = OA_flow;
OA.tent = OA_tent;
OA.coupling = OA_coupling;


fout = 'Sim10_thetaBand.mat';
save(fout, 'OC', 'OA', 'chnames', 'opt');





