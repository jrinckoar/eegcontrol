#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
EEG Control - transfer entropy

sim12: phase-phase

* common ref. average (with mne)
* beta2 band filter (19 - 25 Hz)
* using full length eeg (no windows)
* circular shift surrogates on phases

'Juan Felipe Restrepo <juan.restrepo@uner.edu.ar>'
2022-05-05
===============================================================================
===============================================================================
[SimConfig]
Sim_filename='PP13'
Sim_variables={'C':[1,2]}
Sim_realizations={'SUBJECT':15}
Sim_name='PP13'
Sim_hostname='cluster-fiuner'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=48
#SBATCH --tasks-per-node=48
[endSlurmConfig]
"""
import itertools
import os
import random
import socket
import sys
from datetime import datetime

import mne
import numpy as np
import ray
from knnIMpy.knnTent import knnTent01 as tf
from matplotlib import pyplot as plt
from scipy.io import loadmat, savemat
from scipy.signal import hilbert

try:
    sys.path.append(
        os.path.join(os.path.dirname(__file__), "../../../", "eegCTools"))
    import eegTools
except ImportError:
    sys.path.append(
        os.path.join(os.path.dirname(__file__), "../../", "eegCTools"))
    import eegTools

# if not ray.is_initialized():
#     num_cpus = psutil.cpu_count(logical=False)
#     ray.init(num_cpus=num_cpus)

ray.shutdown()
num_cpus = 48
ray.init(num_cpus=num_cpus)

# Simulation parameters
C = 1
SUBJECT = 1
NSUR = 512
WLENGTH = 5305
# ----------------------------------------------------------------------------
# Change random seed
random.seed()
seed = random.randint(1, 5000)
np.random.seed(seed)
print("seed = {0}".format(seed), flush=True)

# output file
path = os.getcwd()
OUTDIR = path + "/outData/"
if not os.path.isdir(OUTDIR):
    os.makedirs(OUTDIR)
    print("created folder : ", OUTDIR, flush=True)
else:
    print(OUTDIR, "folder already exists.", flush=True)
oname = "PP13_C0{0}".format(C)

# host settings
hostname = socket.gethostname()
if hostname == "apolo":
    # Apolo
    dataPath = "/media/jrestrepo/Datos/eegControl/GrupoControl/mat/"
elif hostname == "zeus":
    # zeus
    dataPath = "/home/jrestrepo/Datos/eegControl/GrupoControl/mat/"
elif hostname == "jupiter":
    # workstation
    dataPath = "/home/jrestrepo/eegcontrol/Datos/mat/"
else:
    # neptuno
    dataPath = "/home/jrestrepo/eegcontrol/Datos/mat/"
sim_opt = {}
sim = {}
sim_opt["seed"] = seed
sim_opt["hostName"] = hostname
# ----------------------------------------------------------------------------
# Simulation
now = datetime.now()
dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")
sim_opt["simDate"] = dt_string_ini
print("------------------------", flush=True)
print("simulation init  " + dt_string_ini, flush=True)
print("------------------------", flush=True)
# ----------------------------------------------------------------------------
# Data
if C == 1:
    condition = "OC"
    dataPath = dataPath + "VigiliaOjosCerrados/"
else:
    condition = "OA"
    dataPath = dataPath + "VigiliaOjosAbiertos/"

if SUBJECT < 10:
    dataFile = dataPath + "ID_0{0}_TF_{1}.mat".format(SUBJECT, condition)
    oname = oname + "_SJ0{0}.mat".format(SUBJECT)
else:
    dataFile = dataPath + "ID_{0}_TF_{1}.mat".format(SUBJECT, condition)
    oname = oname + "_SJ{0}.mat".format(SUBJECT)

data = loadmat(dataFile)
eeg = data["eeg"]
n, nch = eeg.shape
fs = data["fs"]

# create mne object
ch_types = ["eeg"] * nch
ch_names = [x[0][0] for x in data["chanlocs"].T]
info = mne.create_info(ch_names, ch_types=ch_types, sfreq=fs)
eeg = mne.io.RawArray(data["eeg"].T, info)
eeg.set_montage(mne.channels.make_standard_montage("standard_1020"))
sim_opt["lengthEEG"] = eeg._data.shape[0]

# Cortar longitud
eeg._data = eegTools.cropWindows(eeg._data, WLENGTH, SUBJECT - 1, condition)

# plt.figure(num=1)
# eeg.plot()

sim_opt["subject"] = SUBJECT
sim_opt["info"] = data["info"]
sim_opt["condition"] = data["condition"]
sim_opt["chanlocs"] = data["chanlocs"]
sim_opt["fs"] = fs
sim_opt["fsUnits"] = "Hz"
sim_opt["nch"] = nch
sim_opt["dataLength"] = n
sim_opt["nsur"] = NSUR
sim_opt["windlength"] = WLENGTH

# ----------------------------------------------------------------------------
# Set Reference
# common ref. average
eeg.set_eeg_reference(ref_channels="average")
sim_opt["ref"] = "CommonRefAvg"

# # REST reference
# sphere = mne.make_sphere_model("auto", "auto", eeg.info)
# src = mne.setup_volume_source_space(sphere=sphere, exclude=30.0, pos=5.0)
# forward = mne.make_forward_solution(eeg.info, trans=None, src=src, bem=sphere)
# raw_rest = eeg.copy().set_eeg_reference("REST", forward=forward)
# eeg.plot(title='cra')
# plt.show()

# ----------------------------------------------------------------------------
# Filter EEG Bands
l_freq = 19
h_freq = 25
method = "iir"
eeg.filter(l_freq, h_freq, method=method)
sim_opt["FilterType"] = method
sim_opt["Lfreq"] = l_freq
sim_opt["Hfreq"] = h_freq
sim_opt["fband"] = 'beta2'

# test filter
# x = eeg._data[0, :]
# f = np.linspace(-0.5, 0.5, x.size) * fs
# A = np.abs(scipy.fft.fftshift(scipy.fft.fft(x)))
# plt.plot(f.T, A)
# plt.show()

del data
# ----------------------------------------------------------------------------
# Get phase
phase = np.angle(hilbert(eeg._data, axis=1))
del eeg
# ----------------------------------------------------------------------------
# Transfer entropy parameters
m = 4
tau = 7
u = tau
kNearestN = 8
# alpha = getAlpha(kNearestN, 3 * m)
snorm = False
vcor = False
sim_opt["m"] = m
sim_opt["tau"] = tau
sim_opt["u"] = u
sim_opt["kNearestN"] = kNearestN
# sim_opt["alpha"] = alpha
sim_opt["snorm"] = snorm
sim_opt["vcor"] = vcor
# print("alpha done", flush=True)
# ----------------------------------------------------------------------------
# Transfer entropy
# set algorithm parameters
tf_opt = {
    "m": m,
    "tau": tau,
    "u": u,
    "nn": kNearestN,
    # "alpha": alpha,
    "snorm": snorm,
    "vcor": vcor,
}

# channel indices
i = []
j = []
for ch_inds in itertools.combinations(range(nch), 2):
    i.append(ch_inds[0])
    j.append(ch_inds[1])
ch_inds = (i, j)
nrea = int(nch * (nch - 1) / 2)

sim_opt["chinds"] = ch_inds
sim_opt["nrea"] = nrea
# ----------------------------------------------------------------------------

txy = np.zeros((nrea, NSUR))
tyx = np.zeros((nrea, NSUR))

phase_id = ray.put(np.squeeze(phase))
# phase_id = np.squeeze(phase)
Txy_ray_ids = []
Tyx_ray_ids = []
for i, j in zip(*ch_inds):
    Txy_ray_ids.append(eegTools.calc_tent.remote(tf_opt, i, j, phase_id))
    Tyx_ray_ids.append(eegTools.calc_tent.remote(tf_opt, j, i, phase_id))
    # Txy_ray_ids.append(calc_tent(i, j, phase_id, tf_opt))
    # Tyx_ray_ids.append(calc_tent(j, i, phase_id, tf_opt))
txy[:, 0] = np.array(ray.get(Txy_ray_ids))
tyx[:, 0] = np.array(ray.get(Tyx_ray_ids))

print("Normal estimation done", flush=True)

for r in range(1, NSUR):
    ind = np.random.randint(0, n, 1)
    phaseS = eegTools.circularShiftSurrogate(phase, ax=1)
    phase_id = ray.put(np.squeeze(phaseS))

    Txy_ray_ids = []
    Tyx_ray_ids = []
    for i, j in zip(*ch_inds):
        Txy_ray_ids.append(eegTools.calc_tent.remote(tf_opt, i, j, phase_id))
        Tyx_ray_ids.append(eegTools.calc_tent.remote(tf_opt, j, i, phase_id))
    txy[:, r] = np.array(ray.get(Txy_ray_ids))
    tyx[:, r] = np.array(ray.get(Tyx_ray_ids))

    print("{0:.2f} %".format(r / NSUR * 100), flush=True)

sim["data"] = {"txy": txy, "tyx": tyx}
sim["opt"] = sim_opt
savemat(OUTDIR + oname, sim)

print("------------------------", flush=True)
print("Sim start : ", dt_string_ini, flush=True)
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("Sim end: ", dt_string, flush=True)
print("------------------------", flush=True)
